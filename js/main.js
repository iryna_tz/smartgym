(function() {
  var app = angular.module('myApp', []);
    
  app.controller('navController', function(){
    this.products;
  });

  app.directive("navDirective", function() {
    return {
        restrict :'AE',
        template : 
        "<nav class='navbar navbar-inverse'>" + "<div class='container-fluid'>" +
        "<div class='navbar-header'>" + "<a href='#' class='navbar-brand pull-left'><img src='images/logo.png' style='width:30px; hight:30px;'></a>" +
        "<a class='navbar-brand' href='home.html'>SmartGym.com</a>" + "</div>" + 
        "<ul class='nav navbar-nav'>" + 
        "<li class='active'><a href='home.html>Home</a></li>" +
        "<li><a href='workout.html'>Workout</a></li>" +
        "<li><a href='about.html'>About</a></li>" +
        "<li><a href='contactus.html'>Contact</a></li>" + "</ul>" +
        "<ul class='nav navbar-nav navbar-right'>" +
        "<li><a href='#' data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-log-in'></span> Login</a>" +
        "</li>" + "</ul>" + "</div>" + "</nav>"
    }
  });

    
    
    
})();