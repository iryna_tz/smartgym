var app = angular.module('myApp', []);
   
    
    var INTEGER_REGEXP = /^\-?\d*$/;
app.directive('integer', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        if (INTEGER_REGEXP.test(viewValue)) {
          // it is valid
          ctrl.$setValidity('integer', true);
          return viewValue;
        } else {
          // it is invalid, return undefined (no model update)
          ctrl.$setValidity('integer', false);
          return undefined;
        }
      });
    }
  };
});
    
app.controller('formCtrl', function($scope) {
    $scope.master = {name:"", email:"", phone:""};
    
    $scope.update = function(user){
        $scope.master = angular.copy(user);
        alert("Thank you! Your message was sent successfully.");
    };
    
    $scope.isUnchanged = function(user){
        return angular.equals(user, $scope.master);
    }
    
    $scope.reset = function() {
        $scope.user = angular.copy($scope.master);
    };
    $scope.reset();
    
});
     
    }
  });
